//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, TextInput } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

// create a component
const Aircraft = () => {
    return (
        <View style={styles.container}>
            <View style={{backgroundColor:'#c0c0c0', padding:20}}>
               <View style={styles.searchbar}>
                 <MaterialCommunityIcons name="magnify" color={'#c0c0c0'} size={25} />
                 <TextInput placeholder='Aircraft Type' style={{marginTop: -10, fontSize:15,}}/>
               </View>
            </View>
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        //justifyContent: 'center',
        //alignItems: 'center',
        //backgroundColor: '#2c3e50',
    },
    searchbar: {
      //paddingLeft: 10,
      backgroundColor: '#fff',
      padding: 10,
      width: '100%',
      borderRadius: 10,
      flexDirection: 'row',
      paddingVertical: 10,
    },
});

//make this component available to the app
export default Aircraft;
